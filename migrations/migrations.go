package main

import (
	"blchrd/playlistshare/inits"
	"blchrd/playlistshare/models"
)

func init() {
	inits.LoadEnv()
	inits.DBInit()
}

func main() {
	inits.DB.AutoMigrate(&models.Genres{}, &models.Albums{}, &models.Users{})
}
