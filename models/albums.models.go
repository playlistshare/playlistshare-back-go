// models/albums.go

package models

import (
	"time"
)

type Albums struct {
	ID               uint       `json:"id" gorm:"primary_key"`
	Artist           string     `json:"artist"`
	Title            string     `json:"title"`
	Url              string     `json:"url"`
	Note             int        `json:"note"`
	Listened         bool       `json:"listened"`
	ListeningDate    *time.Time `json:"listeningDate" gorm:"column:listeningDate;default:null"`
	Comment          *string    `json:"comment"`
	BandcampItemId   *string    `json:"bandcampItemId" gorm:"column:bandcampItemId"`
	BandcampItemType *string    `json:"bandcampItemType" gorm:"column:bandcampItemType"`
	IsPrivate        bool       `json:"isPrivate" gorm:"column:isPrivate;default:false"`
	CreatedAt        time.Time  `json:"created_at"`
	UpdatedAt        *time.Time `json:"updated_at"`
	Genres           []*Genres  `json:"genre" gorm:"many2many:albums_genres;"`
}
