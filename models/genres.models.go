package models

import (
	"time"
)

type Genres struct {
	ID        uint       `json:"id" gorm:"primary_key"`
	Genre     string     `json:"genre"`
	CreatedAt *time.Time `json:"created_at"`
	UpdatedAt *time.Time `json:"updated_at"`
	Albums    []*Albums  `gorm:"many2many:albums_genres;"`
}
