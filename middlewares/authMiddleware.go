package middlewares

import (
	"blchrd/playlistshare/inits"
	"blchrd/playlistshare/models"
	"fmt"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt/v5"
)

func RequireAuth(context *gin.Context) {
	authorizationHeader := context.Request.Header.Get("Authorization")
	tokens := strings.Split(authorizationHeader, "Bearer ")
	if len(tokens) < 2 {
		context.JSON(401, gin.H{"error": "unauthorized"})
		context.AbortWithStatus(http.StatusUnauthorized)
		return
	}
	tokenString := tokens[1]

	// Keep the cookie version, just in case
	// tokenString, err := context.Cookie("Authorization")
	// if err != nil {
	// 	context.JSON(401, gin.H{"error": "unauthorized"})
	// 	context.AbortWithStatus(http.StatusUnauthorized)
	// 	return
	// }

	token, _ := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}

		return []byte(os.Getenv("SECRET")), nil
	})

	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		if float64(time.Now().Unix()) > claims["exp"].(float64) {
			context.JSON(401, gin.H{"error": "unauthorized"})
			context.AbortWithStatus(http.StatusUnauthorized)
			return
		}

		var user models.Users
		inits.DB.First(&user, int(claims["id"].(float64)))
		if user.ID == 0 {
			context.JSON(401, gin.H{"error": "unauthorized"})
			context.AbortWithStatus(http.StatusUnauthorized)
			return
		}

		context.Set("user", user)
	} else {
		context.AbortWithStatus(http.StatusUnauthorized)
	}
	context.Next()
}
