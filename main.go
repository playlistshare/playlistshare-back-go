// This two tutorials:
// - https://www.honeybadger.io/blog/a-step-by-step-guide-to-creating-production-ready-apis-in-go-with-gin-and-gorm/
// - https://blog.logrocket.com/rest-api-golang-gin-gorm/

package main

import (
	"github.com/gin-gonic/gin"

	"blchrd/playlistshare/controllers"
	"blchrd/playlistshare/inits"
	"blchrd/playlistshare/middlewares"
)

func init() {
	inits.LoadEnv()
	inits.DBInit()
}

func main() {
	publicApiPrefix := "/api/v1/public"
	authenticatedApiPrefix := "/api/v1"
	r := gin.Default()
	r.Use(middlewares.CORSMiddleware())

	// users
	r.POST(authenticatedApiPrefix+"/user", middlewares.RequireAuth, controllers.Signup)
	r.POST(authenticatedApiPrefix+"/login", controllers.Login)
	r.POST(authenticatedApiPrefix+"/logout", controllers.Logout)
	r.POST("/auth", controllers.Validate)
	r.GET("/users", controllers.GetUsers)

	// /albums
	r.GET(publicApiPrefix+"/albums", controllers.FindAlbums)
	r.GET(publicApiPrefix+"/albums/:id", controllers.FindAlbum)
	r.GET(authenticatedApiPrefix+"/albums", middlewares.RequireAuth, controllers.FindAlbums)
	r.GET(authenticatedApiPrefix+"/albums/:id", middlewares.RequireAuth, controllers.FindAlbum)
	r.POST(authenticatedApiPrefix+"/albums", middlewares.RequireAuth, controllers.CreateAlbum)
	r.PUT(authenticatedApiPrefix+"/albums/:id", middlewares.RequireAuth, controllers.UpdateAlbum)
	r.DELETE(authenticatedApiPrefix+"/albums/:id", middlewares.RequireAuth, controllers.DeleteAlbum)
	r.GET(authenticatedApiPrefix+"/bc-details", middlewares.RequireAuth, controllers.GetBCDetails)

	// /genres
	r.GET(publicApiPrefix+"/genres", controllers.FindGenres)

	r.Run()
}
