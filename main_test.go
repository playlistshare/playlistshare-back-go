package main

import (
	"blchrd/playlistshare/controllers"
	"blchrd/playlistshare/inits"
	"blchrd/playlistshare/models"
	"blchrd/playlistshare/resources"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"strconv"
	"strings"
	"testing"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
)

func SetupRouter() *gin.Engine {
	router := gin.Default()
	inits.DBInit("database/dbtest.sqlite")
	inits.DB.AutoMigrate(&models.Genres{}, &models.Albums{}, &models.Users{})
	return router
}

func TestPostAlbumsHandler(t *testing.T) {
	r := SetupRouter()
	r.POST("/albums", controllers.CreateAlbum)

	body := strings.NewReader(`{"url":"https://joneverist.bandcamp.com/album/battletech-original-soundtrack",
								"artist":"Jon Everist",
								"title":"BATTLETECH (Original Soundtrack)",
								"comment":null,
								"listened":false,
								"genre":["VGM","Soundtrack"],
								"note":-1,
								"isPrivate":false}`)
	req, _ := http.NewRequest("POST", "/albums", body)
	req.Header.Set("Content-Type", "application/json")
	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)

	var result struct{ Data resources.AlbumResource }
	json.Unmarshal(w.Body.Bytes(), &result)

	assert.Equal(t, http.StatusCreated, w.Code)
	assert.NotEmpty(t, result)
	assert.Equal(t, "Jon Everist", result.Data.Artist)
	assert.Equal(t, "BATTLETECH (Original Soundtrack)", result.Data.Title)
	assert.Equal(t, []string{"VGM", "Soundtrack"}, result.Data.Genre)
	assert.Equal(t, -1, result.Data.Note)
	assert.Equal(t, false, result.Data.IsPrivate)
	assert.Equal(t, false, result.Data.Listened)
}

func TestGetAlbumsHandler(t *testing.T) {
	r := SetupRouter()
	r.GET("/albums", controllers.FindAlbums)

	//Fill database
	var albums []models.Albums
	timeNow := time.Now()
	albums = append(albums, models.Albums{Artist: "Test", Title: "Test", Url: "https://blchrd.eu", Note: -1, Listened: false})
	albums = append(albums, models.Albums{Artist: "Test2", Title: "Test2", Url: "https://blchrd.eu", Note: -1, Listened: true, ListeningDate: &timeNow})
	albums = append(albums, models.Albums{Artist: "Test3", Title: "Test3", Url: "https://blchrd.eu", Note: -1, Listened: false})
	for _, album := range albums {
		inits.DB.Create(&album)
	}

	req, _ := http.NewRequest("GET", "/albums", nil)
	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)

	var albumsResources resources.PaginatedAlbumResources
	json.Unmarshal(w.Body.Bytes(), &albumsResources)

	assert.Equal(t, http.StatusOK, w.Code)
	assert.NotEmpty(t, albumsResources.Data)
	assert.NotEmpty(t, albumsResources.Links)
	assert.NotEmpty(t, albumsResources.Meta)
}

func TestDeleteAlbumsHandler(t *testing.T) {
	r := SetupRouter()
	r.DELETE("/albums/:id", controllers.DeleteAlbum)

	album := models.Albums{Artist: "Test4", Title: "Test4", Url: "https://blchrd.eu", Note: -1, Listened: false}
	inits.DB.Create(&album)

	req, _ := http.NewRequest("DELETE", "/albums/"+strconv.Itoa(int(album.ID)), nil)
	w := httptest.NewRecorder()

	r.ServeHTTP(w, req)

	//{"data":"album has been deleted successfully"}
	var response struct {
		Data string `json:"data"`
	}
	json.Unmarshal(w.Body.Bytes(), &response)

	assert.Equal(t, http.StatusOK, w.Code)
	assert.Equal(t, "album has been deleted successfully", response.Data)
}

func TestUpdateAlbumsHandler(t *testing.T) {
	r := SetupRouter()
	r.PUT("/albums/:id", controllers.UpdateAlbum)

	album := models.Albums{Artist: "Test5", Title: "Test5", Url: "https://blchrd.eu", Note: -1, Listened: false}
	inits.DB.Create(&album)

	body := strings.NewReader(`{"url":"https://blchrd.eu",
								"artist":"BLCHRD",
								"title":"TestUpdate",
								"comment":"Not that bad",
								"listened":true,
								"listeningDate": "2023-10-17T00:12:24.253Z",
								"genre":["Tech"],
								"note": 3,
								"isPrivate":false}`)

	req, _ := http.NewRequest("PUT", "/albums/"+strconv.Itoa(int(album.ID)), body)
	req.Header.Set("Content-Type", "application/json")
	w := httptest.NewRecorder()

	r.ServeHTTP(w, req)

	listeningDate := time.Time(time.Date(2023, time.October, 17, 0, 12, 24, 253000000, time.UTC))

	var result struct{ Data resources.AlbumResource }
	json.Unmarshal(w.Body.Bytes(), &result)

	assert.Equal(t, http.StatusOK, w.Code)
	assert.NotEmpty(t, result)
	assert.Equal(t, "BLCHRD", result.Data.Artist)
	assert.Equal(t, "TestUpdate", result.Data.Title)
	assert.Equal(t, []string{"Tech"}, result.Data.Genre)
	assert.Equal(t, 3, result.Data.Note)
	assert.Equal(t, false, result.Data.IsPrivate)
	assert.Equal(t, true, result.Data.Listened)
	assert.Equal(t, &listeningDate, result.Data.ListeningDate)
}
