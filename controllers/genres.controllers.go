package controllers

import (
	"net/http"

	"blchrd/playlistshare/inits"
	"blchrd/playlistshare/models"

	"github.com/gin-gonic/gin"
)

// @BasePath /api/v1

// GetGenres godoc
// @Summary Get genres
// @Schemes
// @Description Get genres
// @Tags genre
// @Produce json
// @Success 200 {object} []string
// @Router /genres [get]
func FindGenres(context *gin.Context) {
	var genres []string
	inits.DB.Model(&models.Genres{}).Where("id IN (SELECT DISTINCT genres_id FROM albums_genres)").Order("genre").Distinct().Pluck("genre", &genres)

	context.JSON(http.StatusOK, gin.H{"data": genres})
}
