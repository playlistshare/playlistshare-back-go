package controllers

import (
	"blchrd/playlistshare/inits"
	"blchrd/playlistshare/models"
	"fmt"
	"net/http"
	"os"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt/v5"
	"golang.org/x/crypto/bcrypt"
)

// @BasePath /api/v1

func Signup(context *gin.Context) {
	var body struct {
		Name     string
		Email    string
		Password string
	}

	if context.BindJSON(&body) != nil {
		context.JSON(400, gin.H{"error": "bad request"})
		return
	}

	hash, err := bcrypt.GenerateFromPassword([]byte(body.Password), 10)

	if err != nil {
		context.JSON(500, gin.H{"error": err})
		return
	}

	user := models.Users{Name: body.Name, Email: body.Email, Password: string(hash)}

	result := inits.DB.Create(&user)

	if result.Error != nil {
		context.JSON(500, gin.H{"error": result.Error})
		return
	}

	context.JSON(200, gin.H{"data": user})
}

// Login godoc
// @Summary Logging in the application
// @Schemes
// @Description Try to log in the application
// @Tags user
// @Accept json
// @Produce json
// @Success 200 {object} string
// @Router /login [post]
func Login(context *gin.Context) {
	var body struct {
		Name     string `form:"name"`
		Password string `form:"password"`
	}

	if context.Bind(&body) != nil {
		context.JSON(400, gin.H{"error": "bad request"})
		return
	}

	var user models.Users
	result := inits.DB.Where("email = ?", body.Name).First(&user)

	if result.Error != nil {
		context.JSON(500, gin.H{"error": "User not found"})
		return
	}

	err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(body.Password))

	if err != nil {
		context.JSON(401, gin.H{"error": "unauthorized"})
		return
	}

	// generate jwt token
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"id":  user.ID,
		"exp": time.Now().Add(time.Hour * 24 * 30).Unix(),
	})

	tokenString, err := token.SignedString([]byte(os.Getenv("SECRET")))

	if err != nil {
		context.JSON(500, gin.H{"error": "error signing token"})
		return
	}

	// return the token
	context.JSON(200, gin.H{"access_token": tokenString})

	// keep the cookie version commented, can be useful
	// context.SetSameSite(http.SameSiteLaxMode)
	// context.SetCookie("Authorization", tokenString, 3600*24*30, "", "localhost", false, true)
}

func GetUsers(ctx *gin.Context) {
	var users []models.Users

	err := inits.DB.Model(&models.Users{}).Find(&users).Error

	if err != nil {
		fmt.Println(err)
		ctx.JSON(500, gin.H{"error": "error getting users"})
		return
	}

	ctx.JSON(200, gin.H{"data": users})

}

func Validate(context *gin.Context) {
	user, err := context.Get("user")
	if err {
		context.JSON(500, gin.H{"error": err})
		return
	}
	context.JSON(200, gin.H{"data": "You are logged in!", "user": user})
}

// Logout godoc
// @Summary Logging out
// @Schemes
// @Description Logging out
// @Tags user
// @Accept json
// @Produce json
// @Success 200 {object} string
// @Router /logout [post]
func Logout(context *gin.Context) {
	context.SetSameSite(http.SameSiteLaxMode)
	context.SetCookie("Authorization", "", -1, "", "localhost", false, true)
	context.JSON(200, gin.H{"data": "You are logged out!"})
}
